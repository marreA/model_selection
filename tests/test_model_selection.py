
import unittest
from package.validation_curves import make_data

class Test_model_selection(unittest.TestCase):
    def setUp(self):
        pass
    
    def test_length_data(self):
        self.assertEqual(len(make_data(40)), 2)
